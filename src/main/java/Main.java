import java.util.concurrent.CountDownLatch;

import static java.lang.Thread.sleep;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        CountDownLatch latch = new CountDownLatch(2);


        Thread jas = new Thread(

                () -> {
                    try {
                        System.out.println("Jas - przygotowuje sniadanie.");
                        sleep(500);
                        System.out.println("Jas - bierze prysznic.");
                        sleep(300);
                        System.out.println("Jas - ubiera sie.");
                        sleep(100);
                        System.out.println("Jas - idzie na zakupy.");
                        sleep(1500);
                        System.out.println("Jas - gra na konsoli.");
                        sleep(500);

                        System.out.println("Jas - jestem gotowy.");
                        latch.countDown();
                        latch.await();
                        System.out.println("Jas - idziemy na impreze.");


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        );

        Thread malgosia = new Thread(

                () -> {
                    try {
                        System.out.println("Malgosia - idzie biegac");
                        sleep(600);
                        System.out.println("Malgosia - bierze prysznic");
                        sleep(200);
                        System.out.println("Malgosia - je sniadanie");
                        sleep(100);
                        System.out.println("Malgosia - ubiera sie");
                        sleep(100);
                        System.out.println("Malgosia - spotyka kolezanke");
                        sleep(3500);

                        System.out.println("Malgosia - jestem gotowa");
                        latch.countDown();
                        latch.await();
                        System.out.println("Malgosia - idziemy na impreze");

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        );


        jas.start();
        malgosia.start();


        latch.await();

        System.out.println("---- KONIEC DNIA ----");


    }
}
